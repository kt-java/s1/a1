package com.zuitt.example;

import java.util.Scanner;

public class activity {
    public static void main(String[] args) {
        Scanner myObject = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = myObject.nextLine();

        System.out.println("Last Name: ");
        String lastName = myObject.nextLine();

        System.out.println("First Subject Grade: ");
        float grade1 = myObject.nextFloat();

        System.out.println("Second Subject Grade: ");
        float grade2 = myObject.nextFloat();

        System.out.println("Third Subject Grade: ");
        float grade3 = myObject.nextFloat();

        float average = (grade1 + grade2 + grade3)/3;
        int wholeAverage = (int) average;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + wholeAverage);
    }
}
